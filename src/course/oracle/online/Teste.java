package course.oracle.online;

import java.util.ArrayList;
import java.util.List;

public class Teste {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		List<String> myList = new ArrayList();
		myList.add("Alexandre");
		myList.add("Rosana");
		myList.add("Carol");
		myList.add("Clarinha");
		
		myList.forEach(s -> System.out.println(s));
				
		System.out.println("==================");
		
		myList.replaceAll(s -> s.toUpperCase());
		
		
		myList.forEach(s -> System.out.println(s));
		
		System.out.println("==================");
		
		myList.sort((x,y) -> x.length() - y.length());
		
		myList.forEach(s -> System.out.println(s));
		
	}

}
